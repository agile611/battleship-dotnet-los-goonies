﻿
namespace Battleship.Ascii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Battleship.GameController;
    using Battleship.GameController.Contracts;
    using System.Threading;

    internal class Program
    {
        private static Flota myFleet;
        private static Flota enemyFleet;

        private static List<Position> _misPosiciones;
        private static List<Position> _posicionEnemigas;

        private static Position _ultimaPosicionAmiga;
        private static Position _ultimaPosicionEnemiga;

        static System.Threading.Timer timer = new Timer(TimerCallback, null, System.Threading.Timeout.Infinite, 0);
        static string blinkMessage = "Hello Word";
        static int alertX;
        static int alertY;
        static bool alertDisplayed = false;
        static int cursorX;
        static int cursorY;
        static object consoleLock = new object();

        private static int _turnos = 1;
        private const int MaxTurnosTotales = 200000;

        static void Main()
        {
            _turnos = 1;
            _misPosiciones = new List<Position>();
            _posicionEnemigas = new List<Position>();

            Console.Title = "Welcome to Battleship";
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Clear();
            Console.WriteLine("                                     |__");
            Console.WriteLine(@"                                     |\/");
            Console.WriteLine("                                     ---");
            Console.WriteLine("                                     / | [");
            Console.WriteLine("                              !      | |||");
            Console.WriteLine("                            _/|     _/|-++'");
            Console.WriteLine("                        +  +--|    |--|--|_ |-");
            Console.WriteLine(@"                     { /|__|  |/\__|  |--- |||__/");
            Console.WriteLine(@"                    +---------------___[}-_===_.'____                 /\");
            Console.WriteLine(@"                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _");
            Console.WriteLine(@" __..._____--==/___]_|__|_____________________________[___\==--____,------' .7");
            Console.Write(@"|                        ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Welcome to Battleship                                          ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("BB-61( /");
            Console.WriteLine(@" \_________________________________________________________________________|");
            Console.WriteLine();
            Console.ResetColor();

            InitializeGame();

            StartGame();
        }
        private static void StartGame()
        {
            // Pintamos cañón en base al feedback, sprint 1
            SetColor(ConsoleColor.Yellow);

            Console.Clear();
            Console.WriteLine("                  __");
            Console.WriteLine(@"                 /  \");
            Console.WriteLine("           .-.  |    |");
            Console.WriteLine(@"   *    _.-'  \  \__/");
            Console.WriteLine(@"    \.-'       \");
            Console.WriteLine("   /          _/");
            Console.WriteLine(@"  |      _  /""");
            Console.WriteLine(@"  |     /_\'");
            Console.WriteLine(@"   \    \_/");
            Console.WriteLine(@"    """"""""");

            bool gameOver = false;

            do
            {
                // Controlamos el turno en la cabecera
                Console.Title = "Turn " + _turnos; 

                SetColor(ConsoleColor.Yellow);

                CambioTurno();

                Console.WriteLine("Player, it's your turn");
                Console.WriteLine("Enter coordinates for your shot :");

                // Aquí leemos la posición que ha introducido el usuario, aumentamos turno
                var position = ParsePosition(Console.ReadLine());
                _turnos++;

                Ship isHit = GameController.CheckIsHit(enemyFleet.Ships, position);
                if (isHit != null)  // TOCADO
                {
                    Console.Beep();

                    SetColor(ConsoleColor.Yellow);

                    Console.WriteLine(@"                \         .  ./");
                    Console.WriteLine(@"              \      .:"";'.:..""   /");
                    Console.WriteLine(@"                  (M^^.^~~:.'"").");
                    Console.WriteLine(@"            -   (/  .    . . \ \)  -");
                    Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
                    Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
                    Console.WriteLine(@"                 -\  \     /  /-");
                    Console.WriteLine(@"                   \  \   /  /");

                    Console.WriteLine(isHit.Hundido ? "Sunk!!!!" : "Yeah ! Nice hit !");

                    // Flota enemiga Hundida => acaba la partida
                    if (enemyFleet.FlotaHundida())
                    {
                        GameController.GameOver(true);
                        return;
                    }
                }
                else  // AGUA
                {
                    SetColor(ConsoleColor.Green);
                    Console.WriteLine("Miss");
                }

                SetColor();

                CambioTurno();

                position = GetRandomPosition();

                if (_turnos == 2)
                    position = new Position(Letters.A, 1);
                if (_turnos == 5)
                    position = new Position(Letters.A, 2);

                isHit = GameController.CheckIsHit(myFleet.Ships, position);
                if (isHit != null)
                {
                    SetColor(ConsoleColor.Yellow);
                }
                else
                {
                    SetColor(ConsoleColor.Green);
                }

                // Turno de la máquina
                if (isHit != null)
                {
                    Console.Beep();

                    SetColor(ConsoleColor.Yellow);

                    Console.WriteLine("Computer shot in {0}{1} and {2}", position.Column, position.Row, isHit.Hundido ? "Sunk!!!!" : "Yeah ! Nice hit !");

                    Console.WriteLine(@"                \         .  ./");
                    Console.WriteLine(@"              \      .:"";'.:..""   /");
                    Console.WriteLine(@"                  (M^^.^~~:.'"").");
                    Console.WriteLine(@"            -   (/  .    . . \ \)  -");
                    Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
                    Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
                    Console.WriteLine(@"                 -\  \     /  /-");
                    Console.WriteLine(@"                   \  \   /  /");


                    // Flota enemiga Hundida => acaba la partida
                    if (myFleet.FlotaHundida() || myFleet.Ships.Last().Hundido)
                    {
                        GameController.GameOver(false);
                        gameOver = true;
                    }
                }
                else
                {
                    Console.WriteLine("Computer shot in {0}{1} and {2}", position.Column, position.Row, "miss");

                    SetColor(ConsoleColor.Green);
                }

                SetColor();
            }
            while (!gameOver);

            GameController.GameOver(false);

            //// Temporal hasta que CRIS tenga el método definitivo
            Console.Title = "GAME OVER";
            //Console.WriteLine();
            //Console.WriteLine("GAME OVER");
            //Console.WriteLine();

            Console.WriteLine("¿Desea jugar otra partida? S/N");
            string opcion = Console.ReadLine();

            //// Si el usuario pone Sí, empezamos de nuevo
            if (opcion.Trim().ToLower() == "s")
            {
                Main(); // Volvemos a empezar (pidiendo barcos)
            }
            else
            {
                Console.Clear(); // Ha puesto No, fuera
            }
            // End Temporal
        }

        internal static void WriteBlinkingText(string text, int delay)
        {
            bool visible = true;
            while (true)
            {
                Console.Write("\r" + (visible ? text : new String(' ', text.Length)));
                System.Threading.Thread.Sleep(delay);
                visible = !visible;
            }
        }

        internal static void WriteFlashingText()
        {
            alertX = Console.CursorLeft;
            alertY = Console.CursorTop;
            timer.Change(0, 200);
        }

        internal static void TimerCallback(object state)
        {
            lock (consoleLock)
            {
                cursorX = Console.CursorLeft;
                cursorY = Console.CursorTop;

                Console.CursorLeft = alertX;
                Console.CursorTop = alertY;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.BackgroundColor = ConsoleColor.DarkBlue;

                if (alertDisplayed)
                {
                    Console.WriteLine(blinkMessage);
                }
                else
                {
                    Console.WriteLine("                                                                       ");
                }
                alertDisplayed = !alertDisplayed;
                Console.ResetColor();
                Console.CursorLeft = cursorX;
                Console.CursorTop = Console.CursorTop;
            }
        }

        internal static void SetColor(ConsoleColor color = ConsoleColor.White, ConsoleColor fondo = ConsoleColor.DarkBlue)
        {
            Console.ForegroundColor = color;
            Console.BackgroundColor = fondo;
        }

        internal static void CambioTurno()
        {

            Console.WriteLine();
            Console.WriteLine("======================================================");
            Console.WriteLine();

        }

        internal static Position ParsePosition(string input)
        {
            try
            {
                var letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
                var number = int.Parse(input.Substring(1, 1));
                return new Position(letter, number);

            }
            catch (Exception)
            {
                return new Position(Letters.A,  0);
            }
        }

        private static Position GetRandomPosition()
        {
            int rows = 8;
            int lines = 8;
            var random = new Random();
            var letter = (Letters)random.Next(lines);
            var number = random.Next(rows);
            var position = new Position(letter, number);
            return position;
        }

        private static void InitializeGame()
        {
            Console.WriteLine("¿Desea elegir la ubicación de los barcos del jugador 1? S/N");
            string opcion1 = Console.ReadLine();

            InitializeMyFleet(opcion1.Trim().ToUpper() == "N");

            Console.WriteLine("¿Desea conocer la ubicación de los barcos del jugador 2? S/N");
            string opcion2 = Console.ReadLine();

            InitializeEnemyFleet(opcion2.Trim().ToUpper() == "N");
        }

        private static void InitializeMyFleet(bool random = false)
        {
            myFleet = new Flota(GameController.InitializeShips().ToList());

            SetColor(ConsoleColor.Black, ConsoleColor.White);

            //   Console.WriteLine("Please position your fleet (Game board size is from A to H and 1 to 8) :");

            if (random)
            {
                Random r = new Random(10);

                int row = r.Next(0, 10);

                myFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[1].Positions.Add(new Position { Column = Letters.E, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[1].Positions.Add(new Position { Column = Letters.E, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[1].Positions.Add(new Position { Column = Letters.E, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[1].Positions.Add(new Position { Column = Letters.E, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[2].Positions.Add(new Position { Column = Letters.A, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[2].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[2].Positions.Add(new Position { Column = Letters.C, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[3].Positions.Add(new Position { Column = Letters.F, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[3].Positions.Add(new Position { Column = Letters.G, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[3].Positions.Add(new Position { Column = Letters.H, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[4].Positions.Add(new Position { Column = Letters.C, Row = row });

                row = r.Next(0, 10);
                myFleet.Ships[4].Positions.Add(new Position { Column = Letters.C, Row = row });

                foreach (var ship in myFleet.Ships)
                {

                }
            }
            else
            {
                foreach (var ship in myFleet.Ships)
                {
                    Console.WriteLine();

                    GameController.PaintShip(ship.Size);

                    Console.WriteLine("Please enter the positions for the {0} (size: {1})", ship.Name, ship.Size);
                    for (var i = 1; i <= ship.Size; i++)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine("Enter position {0} of {1} (i.e A3):", i, ship.Size);

                        var posicion = Console.ReadLine();

                        // Comprobamos que la casilla no esté ocupada
                        if (ValidarPosicion(posicion) == ErrorValidacion.None)
                        {
                            ship.AddPosition(posicion);
                        }
                        else
                        {
                            i--;
                            Console.WriteLine("Error position");
                        }
                    }
                }
            }
        }


        private static void InitializeEnemyFleet(bool random = true)
        {
            enemyFleet = new Flota(GameController.InitializeShips().ToList());

            if (random)
            {
                // JT+LJ: PBI 6 (Posicionamiento random de los barcos enemigos)
                Random r = new Random(10);

                int row = r.Next(0, 10);

                enemyFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[0].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[1].Positions.Add(new Position { Column = Letters.E, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[1].Positions.Add(new Position { Column = Letters.E, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[1].Positions.Add(new Position { Column = Letters.E, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[1].Positions.Add(new Position { Column = Letters.E, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[2].Positions.Add(new Position { Column = Letters.A, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[2].Positions.Add(new Position { Column = Letters.B, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[2].Positions.Add(new Position { Column = Letters.C, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[3].Positions.Add(new Position { Column = Letters.F, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[3].Positions.Add(new Position { Column = Letters.G, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[3].Positions.Add(new Position { Column = Letters.H, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[4].Positions.Add(new Position { Column = Letters.C, Row = row });

                row = r.Next(0, 10);
                enemyFleet.Ships[4].Positions.Add(new Position { Column = Letters.C, Row = row });

            }
            else
            {
                // Este es fijo
                enemyFleet.Ships[3].Positions.Add(new Position { Column = Letters.A, Row = 1 });
                enemyFleet.Ships[3].Positions.Add(new Position { Column = Letters.A, Row = 2 });
                enemyFleet.Ships[3].Positions.Add(new Position { Column = Letters.A, Row = 3 });

                enemyFleet.Ships[4].Positions.Add(new Position { Column = Letters.C, Row = 5 });
                enemyFleet.Ships[4].Positions.Add(new Position { Column = Letters.C, Row = 6 });
            }

        }

        private static ErrorValidacion ValidarPosicion(string input)
        {
            try
            {
                var letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
                var number = int.Parse(input.Substring(1, 1));

                var posicion = new Position { Column = letter, Row = number };

                // La posicion la ha añadido antes
                if (_misPosiciones.Contains(posicion))
                    return ErrorValidacion.PosicionYaAñadida;

                // Validar que no salga del tablero
                if (posicion.Row > 8 || posicion.Row < 1)
                    return ErrorValidacion.FueraDeRango;

                if (_ultimaPosicionAmiga != null)
                {
                    // Validamos la posición  de fila y columna
                    if (!ValidarRow(posicion.Row) || !ValidarColumna(posicion.Column))
                        return ErrorValidacion.PosicionNoContigua;
                }

                // Si llega aquí, TODO OK
                _ultimaPosicionAmiga = posicion;
                _misPosiciones.Add(posicion);
                return ErrorValidacion.None;
            }
            catch
            {
                return ErrorValidacion.Excepcion;
            }
        }

        private static bool ValidarColumna(Letters col)
        {
            return true; //Math.Abs(col - _ultimaPosicionAmiga.Column) == 1;
        }

        private static bool ValidarRow(int row)
        {
            return Math.Abs(row - _ultimaPosicionAmiga.Row) == 1;
        }
    }
    internal enum ErrorValidacion
    {
        None = 0, // OK
        FueraDeRango = 1,
        PosicionYaAñadida = 2,
        PosicionNoContigua = 3,
        Excepcion = 4
    }
}

