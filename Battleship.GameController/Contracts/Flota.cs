﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship.GameController.Contracts
{
    public class Flota : List<Ship>
    {
        public List<Ship> Ships;

        public Flota(List<Ship> ships)
        {
            Ships = ships;
        }

        public bool FlotaHundida()
        {
            List<Ship> hundidos = Ships.Where(s => s.Hundido).ToList();
            return Ships.Count == hundidos.Count;
        }
    }
}
